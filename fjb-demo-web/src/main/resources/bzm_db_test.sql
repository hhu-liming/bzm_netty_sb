/*
Navicat MySQL Data Transfer

Source Server         : localhost-mysql8.0
Source Server Version : 80019
Source Host           : localhost:3306
Source Database       : bzm_db_test

Target Server Type    : MYSQL
Target Server Version : 80019
File Encoding         : 65001

Date: 2020-06-06 19:35:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for im_chat_friend_group
-- ----------------------------
DROP TABLE IF EXISTS `im_chat_friend_group`;
CREATE TABLE `im_chat_friend_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `main_user_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `data_status` int DEFAULT '1' COMMENT '数据状态   1、正常     2、删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='朋友组';

-- ----------------------------
-- Records of im_chat_friend_group
-- ----------------------------

-- ----------------------------
-- Table structure for im_chat_friend_info
-- ----------------------------
DROP TABLE IF EXISTS `im_chat_friend_info`;
CREATE TABLE `im_chat_friend_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `main_user_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `friend_group_id` int DEFAULT NULL COMMENT '朋友组id  ',
  `friend_user_id` int DEFAULT NULL COMMENT '朋友用户id',
  `friend_remark` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '朋友备注',
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '好友备注',
  `data_status` int DEFAULT '1' COMMENT '数据状态   1、正常     2、删除',
  `login_status` int DEFAULT '2' COMMENT '登陆状态  1、在线   2、离线 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='im_chat 朋友信息表';

-- ----------------------------
-- Records of im_chat_friend_info
-- ----------------------------

-- ----------------------------
-- Table structure for im_chat_friend_request_logs
-- ----------------------------
DROP TABLE IF EXISTS `im_chat_friend_request_logs`;
CREATE TABLE `im_chat_friend_request_logs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `main_user_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `request_user_id` int DEFAULT NULL,
  `request_status` int DEFAULT '3' COMMENT '请求状态     1、通过     2、已拒绝   3、申请中',
  `request_update_time` datetime DEFAULT NULL,
  `look_status` int DEFAULT '2' COMMENT '查看状态      1、已查看   2、未查看	',
  `look_update_time` datetime DEFAULT NULL COMMENT '查看时间',
  `request_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int DEFAULT NULL,
  `data_status` int DEFAULT '1' COMMENT '数据状态   1、正常     2、删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='好友请求记录';

-- ----------------------------
-- Records of im_chat_friend_request_logs
-- ----------------------------

-- ----------------------------
-- Table structure for im_chat_group_info
-- ----------------------------
DROP TABLE IF EXISTS `im_chat_group_info`;
CREATE TABLE `im_chat_group_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `main_user_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `friend_info_id` int DEFAULT NULL,
  `group_number` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '组编号',
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '组名称',
  `group_tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '组标签',
  `create_user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `group_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '组备注',
  `data_status` int DEFAULT '1' COMMENT '数据状态   1、正常     2、删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='im_chat聊天组信息';

-- ----------------------------
-- Records of im_chat_group_info
-- ----------------------------
INSERT INTO `im_chat_group_info` VALUES ('1', '1992', '1992', null, '8888', '测试群消息', '啦啦、666', '1992', '2020-06-05 13:14:20', null, null, '测试测试', '1');

-- ----------------------------
-- Table structure for im_chat_msg_logs
-- ----------------------------
DROP TABLE IF EXISTS `im_chat_msg_logs`;
CREATE TABLE `im_chat_msg_logs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `logs_type` int DEFAULT '1' COMMENT '日志类型   1、系统消息',
  `main_user_id` int DEFAULT NULL COMMENT '主账号id',
  `user_id` int DEFAULT NULL COMMENT '用户id  消息属于谁的',
  `friend_info_id` int DEFAULT NULL COMMENT '朋友信息表id',
  `group_info_id` int DEFAULT NULL COMMENT '群信息id  当消息类型为 2时',
  `to_type` int DEFAULT '1' COMMENT '发送类型：1、点对点个人消息  2、群消息',
  `send_id` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发送者id',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `receive_id` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接受者id',
  `receive_time` datetime DEFAULT NULL COMMENT '读取时间',
  `msg_type` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消息类型   1 表示文本消息, 2 表示图片， 3 表示语音， 4 表示视频，',
  `msg_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '消息内容',
  `msg_read_status` int DEFAULT '2' COMMENT '消息读状态    1、已读    ，  2、未读',
  `msg_offline_status` int DEFAULT '1' COMMENT '离线消息    1、在线  ， 2、离线',
  `create_time` datetime DEFAULT NULL,
  `create_user_id` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `chat_version` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '聊天信息版本 1.0.0 开始',
  `data_sources` int DEFAULT '1000' COMMENT '数据来源：  1000、电脑网页 （默认）',
  `ip_send_location` varchar(88) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '发送者ip地址',
  `ip_receive_location` varchar(88) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接受者ip地址',
  `check_friend` int DEFAULT '1' COMMENT '是否是好友关系  1、好友   2、不是',
  `data_status` int DEFAULT '1' COMMENT '数据状态   1、正常     2、删除',
  PRIMARY KEY (`id`),
  KEY `idx_main_user_id` (`main_user_id`) USING BTREE,
  KEY `idx_user_id` (`user_id`) USING BTREE,
  KEY `idx_send_id` (`send_id`) USING BTREE,
  KEY `idx_receive_id` (`receive_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='im_chat聊天记录';

-- ----------------------------
-- Records of im_chat_msg_logs
-- ----------------------------
INSERT INTO `im_chat_msg_logs` VALUES ('148', '1', null, null, null, '1', '2', '1992', '2020-06-06 19:18:43', null, null, '1', '啦啦啦啦', '2', '1', '2020-06-06 19:18:43', null, '1.0.0', '1000', null, null, '1', '1');
INSERT INTO `im_chat_msg_logs` VALUES ('149', '1', null, null, null, '1', '2', '1993', '2020-06-06 19:18:58', null, null, '1', '啊啊啊', '2', '1', '2020-06-06 19:18:58', null, '1.0.0', '1000', null, null, '1', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `main_user_id` int NOT NULL COMMENT '主账号id	 ',
  `picture_url` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `user_type` int NOT NULL COMMENT ' 用户类型    超级官员   1000、员工   1001、普通用户    1002',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别    男    ;  女',
  `birthday` date DEFAULT NULL COMMENT '生日	年月日',
  `username` varchar(55) DEFAULT NULL COMMENT '用户名		最长15位 最短8位',
  `password` varchar(55) DEFAULT NULL COMMENT '密码    最长25位 最短8位',
  `phone` varchar(25) DEFAULT NULL COMMENT '手机号',
  `qq` varchar(25) DEFAULT NULL COMMENT 'qq',
  `wechat` varchar(55) DEFAULT NULL COMMENT '微信',
  `email` varchar(55) DEFAULT NULL COMMENT '邮箱',
  `nickname` varchar(35) DEFAULT NULL COMMENT '昵称',
  `nation` varchar(85) DEFAULT NULL COMMENT '国家名族',
  `province` varchar(35) DEFAULT NULL COMMENT '省份',
  `city` varchar(55) DEFAULT NULL COMMENT '市',
  `district` varchar(55) DEFAULT NULL COMMENT '区',
  `street` varchar(85) DEFAULT NULL COMMENT '街道',
  `address` varchar(155) DEFAULT NULL COMMENT '地址',
  `user_status` int DEFAULT NULL COMMENT '用户状态   1、正常     2、停用  ',
  `create_user_id` int DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_user_id` int DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `register_source` varchar(25) DEFAULT NULL COMMENT '注册来源   电脑端 windos ，微信小程序 wx_applet，app  	',
  `register_code` varchar(35) DEFAULT NULL COMMENT '注册码      注册使用的邀请码',
  `invitation_code` varchar(35) DEFAULT NULL COMMENT '邀请码   （用来邀请别人）',
  `description` varchar(255) DEFAULT NULL COMMENT '个人说明',
  `wx_open_id` varchar(88) DEFAULT NULL COMMENT '微信openId',
  `wx_session_key` varchar(88) DEFAULT NULL COMMENT '微信登录会话KEY',
  `latitude` decimal(10,7) DEFAULT NULL COMMENT '纬度  经纬度范围是-180~180',
  `longitude` decimal(10,7) DEFAULT NULL COMMENT '经度  经纬度范围是-180~180',
  `location_update_time` datetime DEFAULT NULL COMMENT '获得 经度 和 纬度 最新时间',
  PRIMARY KEY (`id`),
  KEY `idx_main_user_id` (`main_user_id`) USING BTREE,
  KEY `idx_user_type` (`user_type`) USING BTREE,
  KEY `idx_phone` (`phone`) USING BTREE,
  KEY `idx_user_status` (`user_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2001 DEFAULT CHARSET=utf8 COMMENT='用户信息';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1992', '1992', 'http://www.test.chechaibao.com/image/user/超级帅酷_2001101039286828.png', '1000', '男', '2019-12-22', 'admins', '123456', '17710300418', '68001531', '17710300418', 'hm68001531@163.com', '车解宝', null, null, null, null, null, null, '1', '1992', '2019-12-22 13:45:08', '1992', '2020-01-10 02:39:29', 'windos', '8888', '8888', '我的创业梦', null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('1993', '1993', 'http://www.test.chechaibao.com/images/product/wxe201b96fc8f2d04d_3900.jpg', '1002', '男', '2019-12-25', 'hmsupplier', '123456', '13986076515', '445655046', '17710300418', 'hm68001531@163.com', '冲啊奥巴马', null, null, null, null, null, null, '1', '1992', '2019-12-25 11:32:01', null, null, 'windos', '8888', '6666', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('1995', '1995', 'http://www.test.chechaibao.com/images/product/tmp4e078c913f05d824550bc39ff7005ffc_0776.jpg', '1002', '男', null, null, '123456', '13986075512', null, null, null, '八爪猫', '中国', '湖北省', '武汉市', '江夏区', '兴苑街', '湖北省武汉市江夏区兴苑街', '1', '1995', '2020-02-04 15:46:03', '1995', '2020-03-03 18:52:28', 'wx_applet', null, null, null, 'onyEC5ezCU_Kfy5UFtf1PWkngNfA', 'DyB2yJCE48r6X5TT+/1hDw==', '30.0000000', '114.5384900', '2020-03-03 17:14:22');
INSERT INTO `sys_user` VALUES ('1996', '1996', 'http://www.test.chechaibao.com/images/product/tmp63f65b5274aa13ce544aedfde58e81fe_8728.jpg', '1002', '女', null, null, '123456', '17710300455', null, null, null, '枕头喵', '中国', '湖北省', '武汉市', '江夏区', '兴苑街', '湖北省武汉市江夏区兴苑街', '1', '1996', '2020-02-16 16:28:30', '1996', '2020-03-01 23:01:36', 'wx_applet', null, null, null, 'onyEC5XnY5qfazGnKDeyIaWSEcnc', 'p2gzbfZlYXt3F38opC9GAA==', '30.0000000', '114.5384900', '2020-02-16 16:30:10');
INSERT INTO `sys_user` VALUES ('1997', '1997', 'http://www.test.chechaibao.com/images/product/tmp6a12411875ce488bc32241dac4cc8565_4257.jpg', '1002', '女', null, null, '123456', '13986076512', null, null, null, '母笑阳', '中国', '上海市', '上海市', '浦东新区', '世纪大道', '上海市浦东新区世纪大道8号', '1', '1997', '2020-03-03 17:16:29', '1997', '2020-03-03 17:17:02', 'wx_applet', null, null, null, 'onyEC5Q1JP2lfQm4o2orcWyKaofQ', 'ozYw98oCIWMaLxHCytm1mw==', '30.7500000', '121.8132320', '2020-03-03 17:17:02');
INSERT INTO `sys_user` VALUES ('1998', '1998', 'http://www.test.chechaibao.com/images/product/tmp92d7f05d9f38bed6ddc20de41c83594a_7981.jpg', '1002', '女', null, null, '123456', '13986076511', null, null, null, '邱士贤', null, null, null, null, null, null, '1', '1998', '2020-03-04 17:11:22', null, null, 'wx_applet', null, null, null, 'onyEC5S4N68pkp970kH3dFbEiGew', '1uZPdQivFbmVK1dNXrawTA==', null, null, null);
INSERT INTO `sys_user` VALUES ('1999', '1999', 'http://www.test.chechaibao.com/images/product/wxe201b96fc8f2d04d_3542.jpg', '1002', '男', '2020-06-06', null, '123456', '13986076501', null, null, null, '干掉特朗普', null, null, null, null, null, null, '1', null, null, null, null, 'windos', null, null, null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('2000', '2000', 'http://www.test.chechaibao.com/images/product/tmp4e078c913f05d824550bc39ff7005ffc_0776.jpg', '1002', '男', null, null, '123456', '17710300411', null, null, null, '罗杰斯队长', null, null, null, null, null, null, '1', null, '2020-06-06 16:58:37', null, null, null, null, null, null, null, null, null, null, null);
