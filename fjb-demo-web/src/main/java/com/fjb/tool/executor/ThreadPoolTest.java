package com.fjb.tool.executor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description:自定义线程池，可以用ThreadPoolExecutor类创建，它有多个构造方法来创建线程池，用该类很容易实现自定义的线程池
 * @author hemiao
 * @time:2020年4月13日 下午7:19:16
 */
public class ThreadPoolTest {
	
	/**
	 * 1、从结果中可以看出，七个任务是在线程池的三个线程上执行的。这里简要说明下用到的ThreadPoolExecuror类的构造方法中各个参数的含义。
	 * @Description:TODO
	 * @param args
	 * void
	 * @exception:
	 * @author: hemiao
	 * @time:2020年4月13日 下午7:19:47
	 */
	public static void main(String[] args) {
		// 创建等待队列
		ArrayBlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(20);
		
		// 创建线程池，池中保存的线程数为3，允许的最大线程数为5
		// corePoolSize：线程池中所保存的核心线程数，包括空闲线程。
		// maximumPoolSize：池中允许的最大线程数。
		// keepAliveTime：线程池中的空闲线程所能持续的最长时间。
		// unit：持续时间的单位。		
		// workQueue：任务执行前保存任务的队列，仅保存由execute方法提交的Runnable任务。
		ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 10, 60, TimeUnit.MILLISECONDS, blockingQueue);
		
		// 创建七个任务   
		MyThread t1 = new MyThread();
		MyThread t2 = new MyThread();
		MyThread t3 = new MyThread();
		MyThread t4 = new MyThread();
		MyThread t5 = new MyThread();
		MyThread t6 = new MyThread();
		MyThread t7 = new MyThread();
		
		// 每个任务会在一个线程上执行 
		threadPoolExecutor.execute(t1);
		threadPoolExecutor.execute(t2);
		threadPoolExecutor.execute(t3);
		threadPoolExecutor.execute(t4);
		threadPoolExecutor.execute(t5);
		threadPoolExecutor.execute(t6);
		threadPoolExecutor.execute(t7);
			
		// 关闭线程池  
		threadPoolExecutor.shutdown();
		
	}
	
}
