package com.fjb.service.im;

import com.fjb.pojo.im.ImChatFriendInfo;
import com.fjb.pojo.im.vo.ImChatFriendInfoVo;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * im_chat 朋友信息表 服务类
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatFriendInfoService extends IService<ImChatFriendInfo> {
	
	/**
	 * @Description:查询好友列表
	 * @param userId
	 * @return
	 * List<ImChatFriendInfoVo>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年6月3日 下午1:10:28
	 */
	List<ImChatFriendInfoVo> selectFriendList(Integer userId);

}
