package com.fjb.mapper.im;

import com.fjb.pojo.im.ImChatGroupInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * im_chat聊天组信息 Mapper 接口
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatGroupInfoMapper extends BaseMapper<ImChatGroupInfo> {

}
